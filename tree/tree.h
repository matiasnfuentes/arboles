#include <iostream>

using namespace std;

struct TreeSt{
    int valor;
    TreeSt* ri;
    TreeSt* rd;
};

typedef TreeSt* Tree;


Tree emptyT();
bool isEmptyT(Tree t);
Tree leaf(int x);
Tree branch(int x, Tree ri, Tree rd);
int root(Tree t);
Tree left(Tree t);
Tree right(Tree t);
void destroyT(Tree& t);
