#include <iostream>
#include "tree.h"

using namespace std;



Tree emptyT(){
    return NULL;
}

bool isEmptyT(Tree t){
    return t == NULL;
}

Tree leaf(int x){
    Tree t = new TreeSt;
    t -> valor = x;
    t -> ri = NULL;
    t -> rd = NULL;
    return t;
}

Tree branch(int x, Tree ri, Tree rd){
    Tree t = new TreeSt;
    t -> valor = x;
    t -> ri = ri;
    t -> rd = rd;
    return t;
}

int root(Tree t){
    return t -> valor;
}

Tree left(Tree t){
    return t -> ri;
}

Tree right(Tree t){
    return t -> rd;
}

void destroyT(Tree& t){
    if(t!=NULL){
        destroyT(t->ri);
        destroyT(t->rd);
        delete t;
        t = NULL;
    }
}
