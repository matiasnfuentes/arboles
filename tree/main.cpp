#include <iostream>
#include "tree.h"

using namespace std;



int sumarT(Tree t) {
    int sum = 0;
    if(!isEmptyT(t)){
        sum = root(t) + sumarT(left(t)) + sumarT(right(t));
    }
    return sum;
}

int sizeT(Tree t){
    int tam = 0;
    if(!isEmptyT(t)){
        tam = 1 + sizeT(left(t)) + sizeT(right(t));
    }
    return tam;
}

Tree mapDobleT(Tree t){
    Tree dT = emptyT();
    if(!isEmptyT(t)){
        dT = branch(root(t) * 2,mapDobleT(left(t)),mapDobleT(right(t)));
    }
    return dT;
}

void mapDobleTT(Tree t){
    if(!isEmptyT(t)){
        t -> valor = root(t) * 2;
        mapDobleTT(left(t));
        mapDobleTT(right(t));
    }
}

bool perteneceT(Tree t, int x){
    if(!isEmptyT(t)){
        return (root(t)==x) || perteneceT(left(t), x ) || perteneceT(right(t), x );
    }
    return false;
}

int contar (int x , int y){
    if (x==y){
        return 1;
    }
    else{
        return 0;
    }
}

int aparicionesT(Tree t, int a){
    if(!isEmptyT(t)){
        return contar (root(t), a) + aparicionesT(left(t), a ) + aparicionesT(right(t), a );
    }
    return 0;
}

void printArbol(Tree t){
    if(!isEmptyT(t)){
        cout << root(t) << endl;
        cout << "RI :"<< endl;
        printArbol(left(t));
        cout << "RD :"<< endl;
        printArbol(right(t));
    }
}


int main()
{
    Tree t = emptyT();
    t = branch(5 , branch (2 , NULL, NULL ),branch(5 , NULL, branch(5 ,NULL, NULL ) ) );
    cout << "La suma del arbol es" << endl;
    cout << sumarT(t) << endl;
    cout << "El tama�o del arbol es" << endl;
    cout << sizeT(t) << endl;
    cout << "Apariciones de 5" << endl;
    cout << aparicionesT(t, 5) << endl;
    cout << "pertenece 99" << endl;
    cout << perteneceT(t, 99) << endl;
    cout << "pertenece 2" << endl;
    cout << perteneceT(t, 2) << endl;
    cout << "el doble del arbol es:" << endl;
    t = mapDobleT(t);
    printArbol(t);
    return 0;
}
